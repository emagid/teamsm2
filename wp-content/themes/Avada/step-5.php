<?php
// Template Name: Step 5
get_header(); ?>
	<div id="content" class="full-width">
        
		<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php echo avada_render_rich_snippets_for_pages(); ?>
			<?php echo avada_featured_images_for_pages(); ?>
			<div class="post-content">
				<?php the_content(); ?>
                                <div id="survey" style="text-align:center;display:none;">
<iframe src="https://www.surveymonkey.com/r/WDSL88R" style="min-width:100%;height:600px;" ></iframe>
    </div>
                <style>
    .smcx-embed,
    .smcx-embed>.smcx-iframe-container,
    .survey-page{
        width:100% !important;
        max-width: 100% !important;
    }
                        #main {
        padding-left: 0;
        padding-right: 0;
    }
    #main .avada-row {
        max-width: 100%;
    }
                </style>
                <div class="fusion-fullwidth fullwidth-box" style="background-color:#ea3b2d;background-attachment:scroll;background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;border-color:#eae9e9;border-bottom-width: 0px;border-top-width: 0px;border-bottom-style: solid;border-top-style: solid;padding-bottom:40px;padding-left:0px;padding-right:0px;padding-top:60px;"><div class="avada-row">
<h2 style="text-align: center;"><span style="color: #fff;"><b>BOOK A MEETING NOW</b></span></h2>
<h4 style="text-align: center;"></h4>
</div></div>
                                <!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/carrie-sm2" style="min-width:320px;height:600px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->
                <div class="contact_container">
    <h2>Schedule It Later</h2>
    <?php echo do_shortcode('[contact-form-7 id="1322" title="Schedule Demo"]'); ?>            
</div>


				<?php avada_link_pages(); ?>
			</div>
			<?php if( ! post_password_required($post->ID) ): ?>
			<?php if($smof_data['comments_pages']): ?>
				<?php
				wp_reset_query();
				comments_template();
				?>
			<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
	</div>

                <script type="text/javascript">
jQuery( "a#trig_survey" ).click(function(e) {
    e.preventDefault();
  jQuery( "#survey" ).slideToggle();
});
</script>
<?php get_footer(); ?>