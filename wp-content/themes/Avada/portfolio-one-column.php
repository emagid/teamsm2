<?php
// Template Name: Portfolio One Column
get_header(); ?>
	<div id="content" class="full-width">
        
		<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php echo avada_render_rich_snippets_for_pages(); ?>
			<?php echo avada_featured_images_for_pages(); ?>
			<div class="post-content">
				<?php the_content(); ?>
                <!-- Calendly inline widget begin -->
<div class="calendly-inline-widget" data-url="https://calendly.com/carrie-sm2" style="min-width:320px;height:600px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
<!-- Calendly inline widget end -->
<div class="contact_container">
    <h2>Schedule It Later</h2>
    <?php echo do_shortcode('[contact-form-7 id="1322" title="Schedule Demo"]'); ?>            
</div>
				<?php avada_link_pages(); ?>
			</div>
			<?php if( ! post_password_required($post->ID) ): ?>
			<?php if($smof_data['comments_pages']): ?>
				<?php
				wp_reset_query();
				comments_template();
				?>
			<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
	</div>

<?php get_footer(); ?>

<style>
    #main {
        padding-left: 0;
        padding-right: 0;
        padding-bottom: 0;
    }
    #main .avada-row {
        max-width: 100%;
    }

</style>